#!/usr/bin/env perl
# vim: set tabstop=4 shiftwidth=4 expandtab :
use strict;
use warnings;

use v5.10;
use File::Basename;
use SVN::Client;
use Getopt::Long::Descriptive;

use constant NUM_DIRS => 10;
use constant FILENAME_LEN => 10;
use constant ENTRY_LEN => 10;
use constant LOG_MSG_LEN => 20;
use constant NUM_FILES_PER_DIR => 100;
use constant NUM_REVISIONS => 1000;

my @syms = ('a' .. 'z', 'A' .. 'Z', '0' .. '9');
my ($opt, $usage);

sub random_str {
    join '', @_[ map{ rand @_ } 1 .. shift ];
}

sub log_msg_cb {
    my ($msg, $tmp_file, $commit_items, $pool) = @_;
    if(1 == scalar(@$commit_items) && $$commit_items[0]->state_flags & $SVN::Client::COMMIT_ITEM_IS_COPY) {
        my $i = $$commit_items[0];
        $$msg = "Copy from '" . $i->copyfrom_url() . "' to '" . $i->url() . "'";
    }
    else {
        $$msg = random_str($opt->log_msg_len, @syms, "\n", "\t");
    }
    return 0;
}

sub action_str {
    my $act = shift;
    my $msg;
    if($SVN::Wc::Notify::Action::add == $act) {
        $msg = "add";
    }
    elsif($SVN::Wc::Notify::Action::copy == $act) {
        $msg = "copy";
    }
    elsif($SVN::Wc::Notify::Action::delete == $act) {
        $msg = "delete";
    }
    elsif($SVN::Wc::Notify::Action::restore == $act) {
        $msg = "restore";
    }
    elsif($SVN::Wc::Notify::Action::revert == $act) {
        $msg = "revert";
    }
    elsif($SVN::Wc::Notify::Action::failed_revert == $act) {
        $msg = "failed_revert";
    }
    elsif($SVN::Wc::Notify::Action::resolved == $act) {
        $msg = "resolved";
    }
    elsif($SVN::Wc::Notify::Action::skip == $act) {
        $msg = "skip";
    }
    elsif($SVN::Wc::Notify::Action::update_delete == $act) {
        $msg = "update_delete";
    }
    elsif($SVN::Wc::Notify::Action::update_add == $act) {
        $msg = "update_add";
    }
    elsif($SVN::Wc::Notify::Action::update_update == $act) {
        $msg = "update_update";
    }
    elsif($SVN::Wc::Notify::Action::update_completed == $act) {
        $msg = "update_completed";
    }
    elsif($SVN::Wc::Notify::Action::update_external == $act) {
        $msg = "update_external";
    }
    elsif($SVN::Wc::Notify::Action::status_completed == $act) {
        $msg = "status_completed";
    }
    elsif($SVN::Wc::Notify::Action::status_external == $act) {
        $msg = "status_external";
    }
    elsif($SVN::Wc::Notify::Action::commit_modified == $act) {
        $msg = "commit_modified";
    }
    elsif($SVN::Wc::Notify::Action::commit_added == $act) {
        $msg = "commit_added";
    }
    elsif($SVN::Wc::Notify::Action::commit_deleted == $act) {
        $msg = "comit_deleted";
    }
    elsif($SVN::Wc::Notify::Action::commit_replaced == $act) {
        $msg = "commit_replaced";
    }
    elsif($SVN::Wc::Notify::Action::commit_postfix_txdelta == $act) {
        $msg = "commit_postfix_txdelta";
    }
    elsif($SVN::Wc::Notify::Action::blame_revision == $act) {
        $msg = "blame_revision";
    }
    else {
        $msg = "unknown_action";
    }
    return $msg;
}

sub node_str {
    my $node = shift;
    my $msg;
    if($SVN::Node::none == $node) {
        $msg = "none";
    }
    elsif($SVN::Node::file == $node) {
        $msg = "file";
    }
    elsif($SVN::Node::dir == $node) {
        $msg = "dir";
    }
    elsif($SVN::Node::unknown == $node) {
        $msg = "unknown";
    }
    else {
        $msg = "unexpected";
    }
    return $msg;
}

sub state_str {
    my $st = shift;
    my $msg;
    if($SVN::Wc::Notify::State::unknown == $st) {
        $msg = "unknown";
    }
    elsif($SVN::Wc::Notify::State::unchanged == $st) {
        $msg = "unchanged";
    }
    elsif($SVN::Wc::Notify::State::missing == $st) {
        $msg = "missing";
    }
    elsif($SVN::Wc::Notify::State::obstructed == $st) {
        $msg = "obstructed";
    }
    elsif($SVN::Wc::Notify::State::changed == $st) {
        $msg = "changed";
    }
    elsif($SVN::Wc::Notify::State::merged == $st) {
        $msg = "merged";
    }
    elsif($SVN::Wc::Notify::State::conflicted == $st) {
        $msg = "conflicted";
    }
    else {
        $msg = "unexpected";
    }
    return $msg;
}

sub notify_cb {
    return 0 unless $opt->debug;
    my($file, $action_type, $node_type, $mime_type, $file_state, $revnum) = @_;
    say "NOTIFICATION";
    say "\tFile: '$file'";
    say "\tAction: " . action_str($action_type);
    say "\tNode type: " . node_str($node_type);
    if($mime_type) {
        say "\tMime type: $mime_type";
    }
    else {
        say "\tMime type: undef";
    }
    say "\tFile state: " . state_str($file_state);
    say "\tRevision: $revnum";
    return 0;
}

sub get_random_line_num {
    my $file = shift;
    my $nlines = 0;
    open my $fh, "<$file" or do {
        warn "Can't open $file for reading: $!";
        return 0;
    };
    $nlines++ while(<$fh>);
    close $fh;
    return 0 if 0 == $nlines;
    return int(rand($nlines));
}

sub rm_line {
    my $file = shift;
    my $to_remove = get_random_line_num($file);
	say "Removing line $to_remove from '$file'";
    open my $in, "<$file" or die "Can't open $file for reading: $!";
    my @lines = <$in>;
    close $in;
    open my $out, ">$file" or die "Can't open $file for writing: $!";
    for(my $i = 0; $i <= $#lines; ++$i) {
        print $out $lines[$i] unless $i == $to_remove;
    }
    close $out;
}

sub add_line {
    my $file = shift;
    my $after = get_random_line_num($file);
	say "Adding line to '$file' after line $after";
    open my $in, "<$file" or die "Can't open $file for reading: $!";
    my @lines = <$in>;
    close $in;
    open my $out, ">$file" or die "Can't open $file for writing: $!";
    for(my $i = 0; $i <= $#lines; ++$i) {
        print $out $lines[$i];
        print $out random_str($opt->entry_len, @syms) . "\n" if $i == $after;
    }
    close $out;

}

sub modify_file {
    my $file = shift;
    my $rand = int(rand(4));
    if($rand != 0) {
        add_line($file);
    }
    else {
        rm_line($file);
    }
}

sub modify_dir {
    my $dir = shift;
	say "Modifying files in '$dir'";
    opendir(DIR, $dir) or die "Can't open $dir: $!";
    for my $file (readdir(DIR)) {
        next unless -f "$dir/$file";
        modify_file("$dir/$file");
    }
}

sub create_dirs {
    my $dirs = shift;
    for my $dir(1 .. $opt->ndirs) {
	    say "Creating dir '$dir'";
        mkdir $dir;
        push @$dirs, $dir;
    }
}

sub create_file {
    my $dir = shift;
    my $filename = "$dir/" . random_str($opt->filename_len, @syms);
    say "Creating file '$filename'";
    open my $fh, ">$filename" or do {
        warn "Can't open $filename for writing: $!";
        return;
    };
    print $fh random_str($opt->entry_len - 1, @syms) . "\n";
    close $fh;
}

sub create_files {
    my $dirs = shift;
    for my $dir(@$dirs) {
	    say "Filling up dir '$dir'";
        for(1 .. $opt->files_per_dir) {
            create_file($dir);
        }
    }
}

sub create_repo {
    my ($uri, $config, $client) = @_;
    require SVN::Repos;
    my $path = $uri;
    $path =~ s/^file:\/\///i;
    mkdir $path unless -d $path;
    say "Creating SVN repository at '$path'";
    my $repos = SVN::Repos::create($path, undef, undef, $config, undef);
    say "Creating stdlayout at '$uri'";
    $client->mkdir([ "$uri/trunk", "$uri/branches", "$uri/tags" ]);
}

sub create_branch {
    my($uri, $branch, $client) = @_;
    say "Creating branch '$branch' from 'trunk'";
    $client->copy("$uri/trunk", 'HEAD', "$uri/branches/$branch");
}

sub initial_commit {
    my ($dirs, $client) = @_;
    create_dirs($dirs);

    create_files($dirs);

    for my $dir (@$dirs) {
	    say "Adding dir '$dir' to svn";
        $client->add($dir, 1);
    }

    say "Performing initial commit";
    $client->commit($dirs, 0);
    $client->update('.', 'HEAD', 1);
}

sub find_all_dirs {
    require File::Find::Rule;
    my($root, $dirs) = @_;
    @$dirs = File::Find::Rule->new
                   ->directory
                   ->maxdepth(1)
                   ->not(File::Find::Rule->new->name(qr/^\.\.?$/))
                   ->not(File::Find::Rule->new->name('.svn'))
                   ->in($root);
}

# main
($opt, $usage) = describe_options(
    "$0 %o",
    [ 'repo|r=s',           'repository path', { required => 1 } ],
    [ 'create_repo',             'create repo if it doesn\'t exist' ],
    [ 'branch|b=s',         'branch', { default => 'trunk' } ],
    [ 'create_branch',      'create branch (copy from trunk HEAD), requires --branch' ],
    [ 'path|p=s',           'target checkout dir', { required => 0 } ],
    [ 'revisions|R=i',      'number of revisions to create', { default => NUM_REVISIONS }, ],
    [ 'ndirs|d=i',          'number of dirs to create', { default => NUM_DIRS } ],
    [ 'files_per_dir|f=i',  'number of files in directory', { default => NUM_FILES_PER_DIR } ],
    [ 'filename_len|l=i',   'length of filenames', { default => FILENAME_LEN } ],
    [ 'entry_len|e=i',      'length of an entry in file', { default => ENTRY_LEN } ],
    [ 'log_msg_len|l=i',    'length of log message', { default => LOG_MSG_LEN } ],
    [ 'update_existing',    'do not create anything, update existing files' ],
    [ 'debug',              'print debug info' ],
    [ 'help',               'print usage message and exit' ],
    );

say $usage->text, exit 0 if $opt->help;

my $client = SVN::Client->new(
            log_msg => \&log_msg_cb,
            notify  => \&notify_cb,
        );
my $conf_dir = undef;
my $conf = SVN::Core::config_get_config($conf_dir);

my($repo, $path);

if(SVN::Core::path_is_url($opt->repo)) {
    $repo = SVN::Core::uri_canonicalize($opt->repo);
}
else {
    $repo = SVN::Core::dirent_canonicalize($opt->repo);
}

create_repo($repo, $conf, $client) if $opt->create_repo;

if($opt->create_branch) {
    die "Attempt to create branch without a name" unless exists($$opt{branch});
    create_branch($repo, $opt->{branch}, $client);
}

$path = (exists $$opt{path}) ? $opt->path : './' . basename($repo);
$path .= "/$opt->{branch}";

{
    my $uri = $repo;
    if($opt->{branch} ne 'trunk') {
        $uri .= '/branches';
    }
    $uri .= "/$opt->{branch}";
    say "Checking out '$uri' to '$path'";
    $client->checkout($uri, $path, 'HEAD', 1);
}
chdir $path;

my @dirs;

if($opt->update_existing || $opt->create_branch) {
    find_all_dirs('.', \@dirs);
}
else {
    initial_commit(\@dirs, $client);
}

for my $rev(1 .. $opt->revisions) {
    for my $dir(@dirs) {
        modify_dir($dir);
    }
	say "Committing revision $rev";
    my $res = $client->commit(\@dirs, 0);
    if($SVN::Core::INVALID_REVNUM == $res) {
        say "Nothing was committed, redoing work";
        redo;
    }
    $client->update('.', 'HEAD', 1);
}
